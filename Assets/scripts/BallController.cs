﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {
	int score = 0;
    public Text myText; // открытая, общедост перем
    public Text scoreText;
    private bool gameOver = false; // закрытая перем
	

    void OnCollisionEnter(Collision other)
    {
        if (!gameOver && other.gameObject.tag == "Terrain")
        {
            // Debug.Log("Game over");
            print("Game Over");
            myText.enabled = true;
            // Debug.Log("Game over"); 
            print("Game Over");
            gameOver = true;
            Destroy(gameObject);

        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (!gameOver && other.gameObject.tag == "Finish")
        {
            print("You win!");

            myText.color = Color.green;
            myText.text = "You\nwin";
            myText.enabled = true;
            gameOver = true;
        }

        if (!gameOver && other.gameObject.tag.Contains("Teapot"))
        {
            switch (other.gameObject.tag)
            {
                case "TeapotBronze":
                    score++;
                    break;
                case "TeapotSilver":
                    score += 2;
                    break;
                case "TeapotGold":
                    score += 3;
                    break;
            }

            Destroy(other);
            scoreText.text = "SCORE = " + score;
            other
                .gameObject
                .GetComponent<Animator>() // получить компонент GO
                .Play("collect");
            Destroy(other.gameObject, 1.0f);
        }
    }
}
