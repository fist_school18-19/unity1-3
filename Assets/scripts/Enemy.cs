﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum States
{
    Idle, // = 0
    Walk, // = 1
    Run,
    Attack,
    Damage,
    KnockBack,
    Death
}

public class Enemy : MonoBehaviour
{
    [SerializeField] int health;
    PlayerController pc;
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        // damage&death
        if (animator.GetInteger("state") > (int)States.Attack
            && animator.GetCurrentAnimatorStateInfo(0)
                .normalizedTime < 1
            )
            return;
        // attack
        if (pc)
            // walk or attack
            if (Vector3.Magnitude(
                    pc.transform.position -
                    transform.position
                ) <= 1f )
                // attack
                animator.SetInteger("state", (int)States.Attack);
            else
                // walk
                animator.SetInteger("state", (int)States.Walk);
        // idle
        else
            animator.SetInteger("state", (int)States.Idle);

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            pc = other.GetComponent<PlayerController>();
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            pc = null;
    }
    public void GetDamage(int dmg)
    {
        animator.SetInteger("state", (int)States.Damage);
        health -= dmg;
        if (health <= 0)
        {
            animator.SetInteger("state", (int)States.Death);
            Destroy(this);
        }
    }
}
