﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] string path = "/world.txt";
    [HideInInspector] public GameObject player;
    [HideInInspector] public LandscapeGenerator landscapeGenerator;

    bool create = true;

    // шаблон проектирования "одиночка", singleton
    public static GameManager GetInstance() { return _instance; }
    private static GameManager _instance;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (_instance == null)
            _instance = this;
    }

    public void LoadLevel(bool needCreate)
    {
        create = needCreate;
        SceneManager.LoadScene(1);
    }
    public void CloseGame()
    {
        landscapeGenerator.Save(Application.dataPath + path);
        Application.Quit();
    }
    void OnLevelWasLoaded(int level)
    {
        if (create)
            landscapeGenerator.Create();
        else
            landscapeGenerator.Load(Application.dataPath + path);
    }
}
