﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    [SerializeField] int hp;
    [SerializeField] GameObject destroyed;
    public void GetDamage(int dmg) {
        hp -= dmg;
        if (hp <= 0) {
            if (destroyed)
                GameObject.Instantiate(
                    destroyed,
                    transform.position,
                    transform.rotation,
                    transform.parent
                );
            Destroy(gameObject);
        }
    }
}
