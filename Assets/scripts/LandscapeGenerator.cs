﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class LandscapeGenerator : MonoBehaviour
{
    [SerializeField] int size;
    public GameObject[] prefabs;
    [SerializeField] int maxHeight;
    void Awake()
    {
        GameManager.GetInstance().landscapeGenerator = this;
    }
    void OnDestroy()
    {
        //Save();
    }
    public void Create() {
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                int y = (int)(maxHeight * Mathf.PerlinNoise(i / 32f, j / 32f));
                //while (y >= 0)
                //{
                GameObject.Instantiate(
                    prefabs[(int)(y * prefabs.Length * (1f / maxHeight))],
                    new Vector3(i, y, j),
                    Quaternion.identity,
                    transform
                );
                // y--;
                //}
            }
        }
    }
    public void Save(string path)
    {
        List<string> lines = new List<string>();
        foreach (Transform block in transform)
        {
            if (!block.GetComponent<ID>())
                continue;
            string tmp = "";
            tmp += block.GetComponent<ID>().id + "; ";
            tmp += block.transform.position.x + "; ";
            tmp += block.transform.position.y + "; ";
            tmp += block.transform.position.z + "; ";

            lines.Add(tmp);
        }
        File.WriteAllLines(path, lines.ToArray());
    }
    public void Load(string path)
    {
        string[] lines = File.ReadAllLines(path);
        foreach (string line in lines) // "1; 2; 3; 4;"
        {
            string[] values = line.Split(';');
            int id = int.Parse(values[0]);
            int x = int.Parse(values[1]);
            int y = int.Parse(values[2]);
            int z = int.Parse(values[3]);

            GameObject.Instantiate(
                    prefabs[id + 1],
                    new Vector3(x, y, z),
                    Quaternion.identity,
                    transform
                );
        }
        for (int j = 0; j < size; j++)
            for (int i = 0; i < size; i++)
                GameObject.Instantiate(
                        prefabs[0],
                        new Vector3(i, 0, j),
                        Quaternion.identity,
                        transform
                    );
    }
}
