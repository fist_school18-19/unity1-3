﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Animator anim;
    Camera cam;
    int[] MatsCount = new int[5];
    int[] BlocksCount = new int[5];
    int selectedBlock = 0;

    [SerializeField] RectTransform inventoryP;
    [SerializeField] RectTransform qp;
    [SerializeField] RectTransform selector;

    [SerializeField] GameObject slotPrefab;
    [SerializeField] Transform phantomBlock;
    [SerializeField] LandscapeGenerator lg;

    void Start()
    {
        GameManager.GetInstance().player = gameObject;

        cam = GetComponentInChildren<Camera>();
        anim = GetComponentInChildren<Animator>();
    }
    void Update()
    {
        RaycastHit hit2 = new RaycastHit();
        Physics.Raycast(
            cam.transform.position,
            cam.transform.forward,
            out hit2,
            5.0f
        );
        if (hit2.collider && hit2.collider.tag == "Block")
        {
            phantomBlock.position =
                hit2.transform.position +
                hit2.normal;
            phantomBlock.GetComponent<MeshRenderer>()
                .enabled = true;
        }
        else
        {
            phantomBlock.GetComponent<MeshRenderer>()
                .enabled = false;
        }

        if (Input.GetMouseButtonDown(0) && anim != null)
        {
            anim.SetTrigger("Attack");
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(
                cam.transform.position,
                cam.transform.forward,
                out hit,
                5.0f
            );
            Health h;
            //есть данные not null -> bool => true
            //нет, пустое значение null -> bool => false
            // hit.collider != null <=> hit.collider
            if (hit.collider && (h = hit.collider.gameObject
                .GetComponent<Health>()))
            {
                h.GetDamage(10);
            }
            Enemy e;
            if (hit.collider && (e = hit.collider.gameObject
                .GetComponent<Enemy>()))
            {
                e.GetDamage(10);
            }
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(
                cam.transform.position,
                cam.transform.forward,
                out hit,
                5.0f
            );
            if (hit.collider != null &&
                hit.collider.gameObject.tag == "Weapon")
            {
                //drop weapon
                DropWeapon();
                //pick up weapon
                qp
                    .GetChild(5).GetChild(0)
                    .GetComponent<Image>()
                    .sprite = 
                hit.collider.GetComponent<Weapon>()
                    .icon;

                hit.collider.transform
                    .SetParent(transform.GetChild(1));

                hit.collider.transform
                    .GetComponent<Animator>()
                    .enabled = true;
                anim = hit.collider.transform
                    .GetComponent<Animator>();

                hit.collider.transform
                    .localPosition = Vector3.zero;
                hit.collider.transform
                    .localRotation = Quaternion.identity;
                hit.collider.transform
                    .GetComponent<Rigidbody>()
                    .isKinematic = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            DropWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ShowInventory();
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (phantomBlock.GetComponent<MeshRenderer>().enabled
                && BlocksCount[selectedBlock] > 0
                )
            {
                GameObject.Instantiate(
                    lg.prefabs[selectedBlock + 1],
                    phantomBlock.position,
                    phantomBlock.rotation,
                    lg.transform
                    );
                ChangeBlocks(selectedBlock, -1);
            }
        }
                
        selectedBlock = (MatsCount.Length + (selectedBlock += System.Math.Sign(Input.GetAxis("Mouse ScrollWheel")))) % MatsCount.Length;

        selector.SetParent(qp.GetChild(6 + selectedBlock));
        selector.localPosition = Vector3.zero;
    }
    void DropWeapon()
    {
        if (transform.GetChild(1).childCount > 0)
        {
            transform.GetChild(1).GetChild(0)
                .GetComponent<Rigidbody>()
                .isKinematic = false;
            transform.GetChild(1).GetChild(0)
                .GetComponent<Animator>()
                .enabled = false;
            transform.GetChild(1).GetChild(0)
                .SetParent(null);
        }
    }
    public void ChangeMats(int id, int value)
    {
        MatsCount[id] += value;
        qp.GetChild(id).GetComponentInChildren<Text>().text =
            MatsCount[id].ToString();
    }
    public void ChangeBlocks(int id, int value)
    {
        BlocksCount[id] += value;
        qp.GetChild(6 + id).GetComponentInChildren<Text>().text =
            BlocksCount[id].ToString();
    }
    void ShowInventory()
    {
        bool current = inventoryP.gameObject.activeInHierarchy;

        inventoryP.gameObject.SetActive(!current);
        GetComponent<UnityStandardAssets
            .Characters.FirstPerson
            .FirstPersonController>().enabled= current;
        if (!current) Cursor.lockState = CursorLockMode.None;
        Cursor.visible = !current;
        Time.timeScale = current ? 1 : 0;

        if (!current)
        {
            for (int i = 0; i < MatsCount.Length; i++)
            {
                for (int j = 0; j < MatsCount[i]; j++)
                {
                    GameObject go = GameObject.Instantiate(
                        slotPrefab,
                        inventoryP
                            .GetComponentInChildren<GridLayoutGroup>()
                            .transform
                    );
                    go.transform.GetChild(0).GetComponent<Image>()
                        .sprite =
                        qp.GetChild(i).GetChild(0).GetComponent<Image>()
                            .sprite;
                }
            }
        }
        else
        {
            foreach (Transform t in inventoryP
                .GetComponentInChildren<GridLayoutGroup>()
                .transform)
            {
                Destroy(t.gameObject);
            }
            inventoryP.GetComponent<UIManager>().Clean();
        }

    }
}
