﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public RectTransform[] craftSlots;
    int[] blender = new int[5];
    [SerializeField] GameObject player;
    public void Craft()
    {
        foreach (RectTransform slot in craftSlots)
        {
            if (slot.childCount > 0)
            {
                int id = int.Parse(slot.GetChild(0)
                    .GetComponent<Image>()
                    .sprite.name);
                blender[id]++;
            }
        }
        int blockID = System.Array.IndexOf(blender, 4);
        if (blockID < 0) return;

        //craft here:
        player.GetComponent<PlayerController>().ChangeMats(blockID, -4);
        player.GetComponent<PlayerController>().ChangeBlocks(blockID, 1);

        //clean blender:
        Clean();
    }
    public void Clean()
    {
        foreach (RectTransform slot in craftSlots)
        {
            if (slot.childCount > 0)
            {
                Destroy(slot.GetChild(0).gameObject);
            }
        }
        blender = new int[5];
    }
}
